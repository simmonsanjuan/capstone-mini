import React, {Fragment} from 'react';
import {Button, Form, FormGroup, Label, Input, FormText, Container, Col, Row} from 'reactstrap';
import {Link} from 'react-router-dom'



const CategoryPage = (props) => {
	return (
		<Fragment>
			<Container className = "my-5">
				<Row className = "mb-3">
					<Col>
						<h1 className="text-center mt-5">Choose a Category:</h1>
					</Col>
				</Row>
			</Container>

			<Container className="ml-auto">
				<Row className = "mb-3">
					<Col className = "md-4">
						<Button className="btnCategory btn-primary ml-1">
							<Link to="/inlove">IN LOVE</Link>
						</Button>
					</Col>

					<Col className = "md-4">
						<Button className="btnCategory btn-primary ml-1">
							<Link to="/hungry">HUNGRY</Link>
						</Button>
					</Col>

					<Col className = "md-4">
						<Button className="btnCategory btn-primary ml-1">
							<Link to="/pagod">PAGOD</Link>
						</Button>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}

export default CategoryPage;