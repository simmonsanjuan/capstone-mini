import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const NotFoundPage = (props) => {
  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Page Not Found</h1>
        </Col>
      </Row>
    </Container>
  );
}

export default NotFoundPage;