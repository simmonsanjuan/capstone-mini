import React, {Fragment} from 'react';
import { Container, Row, Col } from 'reactstrap';
import DamingTimeForm from '../forms/DamingTimeForm';

const DamingTimePage = (props) => {
	return (
		<Fragment>
			<Container className = "my-5">
				<Row className = "mb-3">
					<Col>
						<h1>What's on your mind? </h1>
					</Col>
				</Row>

				<Row>
				<Col>
				<DamingTimeForm/>
				</Col>
				</Row>
			</Container>
		</Fragment>
	)
}

export default DamingTimePage;
